const moviesData = require('../dataset/movies.json');
const cantidad = moviesData.length;
console.log("[+] Cantidad de electrónes almacenados: "+cantidad);
const express = require('../node_modules/express');
const bodyParser = require('../node_modules/body-parser');
//console.log(moviesData);

// Función para realizar comparaciones entre ratings (ordena de menor a mayor):
const ordenamiento = (tipo) =>{
    return (movie1,movie2) =>{
        let resultado = (movie1[tipo] < movie2[tipo]) ? -1 : (movie1[tipo] > movie2[tipo]) ? 1 : 0;
        return resultado;
    }
}

const puertoTCP = 25674;
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.get('/', (req,res) =>{
    res.status(200).json({mensaje: "[+] Iniciando colmena de computadores cuánticos"});
});
app.get('/movies', (req,res) =>{
    res.status(200).json({cantidad: cantidad, movies: moviesData});
});

app.get('/movies/:name', (req,res) =>{
    let claveBusqueda = req.params.name;
    if(typeof claveBusqueda !== "string" || claveBusqueda === " ")
        res.status(500).json({mensaje: "El nombre ingresado no sirve"});
    let resultados = [];
    for(let i=0; i < moviesData.length; i++){
        let title = moviesData[i].Title;
        try{
            if(typeof title !== "string"){
                if(typeof title === "object" && JSON.stringify(title) === "null")
                    title = "";
                if(typeof title !== "null")
                    title = title.toString();
                else
                    title = "";
            }
            let lowTitle = title.toLowerCase();
            if(lowTitle.includes(claveBusqueda.toLowerCase())){
                console.log(title);
                resultados.push(title);
            }
        }
        catch(err){
            console.log("[!] Error: " + err);
        }
    }
    if(resultados.length == 0){
        res.status(200).json({status: 200,
                              message: "No se encontró el título buscado"});
    }
    else{
        res.status(200).json({status: 200,
                              data: resultados});
    }
});

app.get('/movies/rating/:classifier/:order', (req,res) =>{
    /* Notas:
        IMDB Rating -> Número Flotante 0-10 / null
        Rotten Tomatoes Rating -> Número entero 0-100 / null

        Se retorna: 
        {status: "<code>",
         data: "<array-ordenado>",
         sinInfo: "<array-con-respectivo-clasificador-nulo>"}
    */
    let classifier = req.params.classifier;
    let order = req.params.order;
    let listaOrdenamiento = [];
    let listaNulos = [];
    let resultados = [];
    if(order === "asc"){
        for(let i=0; i < moviesData.length; i++){
            let rating = moviesData[i][classifier];
            if(typeof rating === "number")
                listaOrdenamiento.push(moviesData[i]);
            else
                listaNulos.push(moviesData[i]);
        }
        // Ordenamos de menor a mayor
        listaOrdenamiento.sort(ordenamiento(classifier));

        // Agregamos la lista ordenada a los resultados
        resultados = listaOrdenamiento;
    }
    else if(order === "desc"){
        for(let i=0; i < moviesData.length; i++){
            let rating = moviesData[i][classifier];
            if(typeof rating === "number")
                listaOrdenamiento.push(moviesData[i]);
            else
                listaNulos.push(moviesData[i]);
        }
        // Ordenamos de menor a mayor
        listaOrdenamiento.sort(ordenamiento(classifier));
        
        // Invertimos orden, y agregamos a resultados
        resultados = listaOrdenamiento.reverse();
    }
    else{
        res.status(500).json({error: "Tipo de Ordenamiento no válido"});
        return ;
    }

    if(resultados.length == 0){
        res.status(200).json({status: 200,
                              message: "No se encontró el título buscado"});
    }
    else{
        res.status(200).json({status: 200,
                              data: resultados,
                              sinRating: listaNulos});
    }
});

app.listen(puertoTCP, () =>{
    console.log("[+] Autodestrucción activada.");
});

